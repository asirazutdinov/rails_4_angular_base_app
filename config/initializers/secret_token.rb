# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Rails4AngularBaseApp::Application.config.secret_key_base = 'fbf6a656d085c7f8ec6e551e769ac42d767fc480372b052eb6dc4f2d4826c5a6af555c91cde4ed9e511a29860ccc5e4e8ee224d353eb7e245590060fff57bee1'
